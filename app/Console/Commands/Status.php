<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\TransferException;
use Illuminate\Console\Command;



class Status extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:ping {--url=} {--proxy=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check website status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws GuzzleException
     */

    public function handle()
    {
        $client = new Client();
        try {
            $response = $client->get('http://' . $this->option('url'), [ 'http_errors' => false, 'curl' => [CURLOPT_PROXYTYPE => 7], 'verify' => true, 'proxy' => $this->option('proxy'), 'timeout' => 10 ]);
            $client->put('http://82.118.23.8:9091/metrics/job/' . $this->option('url'), ['body' => '#'. "" .'HELP http_status Show status remote site' . "\n" . '#' . "" .'TYPE http_status gauge' . "\n" . 'http_status ' .  intval($response->getStatusCode()) . "\n"]);
        } catch (TransferException $e) {
            $response = $e->getHandlerContext();
            $err = $response['errno'];
            if ((isset($err) && ($err == '28'))) {
                    $client->put('http://82.118.23.8:9091/metrics/job/' . $this->option('url'), ['body' => '#'. "" .'HELP http_status Show status remote site' . "\n" . '#' . "" .'TYPE http_status gauge' . "\n" . 'http_status ' . '10' . "\n"]);
                }
            }
    }
}
